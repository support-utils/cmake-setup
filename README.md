# CMake Setup #

This project is used to describe the basic setup for a simple CMake project, including a description on how to use it on different machines.

The `CMakeLists.txt` file can be used to create a simple CMake project. Make sure to change the project name into anything you like. It is common to rename the file containing the main function into the same name you give to the project.

## Windows ##

Obtaining a C/C++ compiler in Windows can be quite a struggle. Visual Studio provides such a compiler, which can be used in combination with any other IDE, like VS Code.

First, Visual Studio must be installed by using the dedicated [installer](https://visualstudio.microsoft.com/downloads/). Next by executing the installer, you will soon be greeted with a screen listing a number of _workloads_, make sure to select the  _Desktop development with C++_ workload. By doing so, you will install the required components which will provide you with the compiler. After this, you are set and done.

## VS Code ##

CMake and VS Code go hand-in-hand, or at least, if you install the [cmake tools extension](https://marketplace.visualstudio.com/items?itemName=ms-vscode.cmake-tools) provided by Microsoft. Don't forget to enable support for C/C++ by installing the [C/C++ extension](https://marketplace.visualstudio.com/items?itemName=ms-vscode.cpptools). 

For windows you can copy the `.vscode` directory from this repository to start directly developing. Inside the `launch.json` make sure that the executable matches the project name.